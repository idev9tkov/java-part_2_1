package ru.itdrive.app;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
@Parameters(separators = "=")
public class Args {
    @Parameter(names = {"--port"})
    public int port;

    @Parameter(names = {"--serverPort"})
    public int serverPort;

    @Parameter(names = {"--serverHost"})
    public String serverHost;
}
