package ru.itdrive.sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class EchoServerSocket {

    private Socket socketClient;
    private List<Socket> socketsList = new ArrayList<>();

    public void start(int port) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
                try {
                    while (true) {
                        socketClient = serverSocket.accept();
                        socketsList.add(socketClient);
                        new Thread(() -> {
                            try {
                                InputStream clientInputStream = socketClient.getInputStream();
                                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
                                System.out.println("Client Connected - " + Thread.currentThread().getName());
                                String inputLine = clientReader.readLine();
                                while (inputLine != null) {
                                    for (Socket value : socketsList) {
                                        PrintWriter writer = new PrintWriter(value.getOutputStream(), true);
                                        writer.println(inputLine);
                                    }
                                    inputLine = clientReader.readLine();
                                }
                            } catch (IOException e) {
                                throw new IllegalArgumentException(e);
                            }
                        }).start();
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
