package ru.itdrive.sockets;

import com.beust.jcommander.JCommander;
import ru.itdrive.app.Args;

import java.util.Scanner;

public class MainForClient {
    public static void main(String[] args) {
        Args arguments = new Args();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        Scanner scanner = new Scanner(System.in);
//        SocketClient client = new SocketClient("127.0.0.1", 7777);
        SocketClient client = new SocketClient(arguments.serverHost, arguments.serverPort);

        while (true) {
            String message = scanner.nextLine();
            client.sendMessage(message);
        }
    }
}
