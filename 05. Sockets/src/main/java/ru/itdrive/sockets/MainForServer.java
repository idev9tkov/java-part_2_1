package ru.itdrive.sockets;

import com.beust.jcommander.JCommander;
import ru.itdrive.app.Args;

public class MainForServer {
    public static void main(String[] args) {
        Args arguments = new Args();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        EchoServerSocket serverSocket = new EchoServerSocket();
        serverSocket.start(arguments.port);
    }
}
