package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.models.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CourseRepositoryJdbcImpl implements CourseRepository{
    //language=SQL
    private static final String SQL_SELECT_ALL = "select c.id, l.id, l.name, l.course_id from course c inner join lesson l on c.id = l.course_id";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select c.id, l.name, l.course_id\n" +
            "from course c\n" +
            "         inner join lesson l on c.id = l.course_id where c.id = ";

    private Connection connection;

    public CourseRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Course object) {
    }

    public void update(Course object) {
    }

    public void delete(Integer id) {
    }

    public Course find(Integer id) {
        try {
            Course course = new Course();
            List<Lesson> lessons = new ArrayList<Lesson>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("course_id")));
            }
            course.setId(id);
            course.setLessons(lessons);
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Course> findAll() {
        try {
            List<Course> courses = new ArrayList<Course>();
            Course course = new Course(1);
            List<Lesson> lessons = new ArrayList<Lesson>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                if (!course.getId().equals(id)) {
                    course.setLessons(lessons);
                    courses.add(course);
                    course = new Course();
                    lessons = new ArrayList<Lesson>();
                    course.setId(id);
                    lessons.add(new Lesson(
                            resultSet.getInt(2),
                            resultSet.getString("name"),
                            resultSet.getInt("course_id")));
                } else {
                     lessons.add(new Lesson(
                             resultSet.getInt(2),
                             resultSet.getString("name"),
                             resultSet.getInt("course_id")));
                }
            }
            course.setLessons(lessons);
            courses.add(course);
            return courses;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
