package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Course;

public interface CourseRepository extends CrudRepository<Course> {
}
