package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson> {
}
