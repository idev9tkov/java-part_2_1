package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Student;

public interface StudentsRepository extends CrudRepository<Student> {
    Student findByFirstName(String firstName);
}
