package ru.itdrive.jdbc.repositories;

import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.models.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonRepositoryJdbcImpl implements LessonRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from lesson";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from lesson where id = ";


    private Connection connection;

    private RowMapper<Lesson> lessonRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getInt("course_id")
            );
        }
    };


    public LessonRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Lesson object) {

    }

    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            return lessonRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Lesson> findAll() {
        try {
            List<Lesson> result = new ArrayList<Lesson>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Lesson lesson = lessonRowMapper.mapRow(resultSet);
                result.add(lesson);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
