package ru.itdrive.jdbc;

import ru.itdrive.jdbc.models.Course;
import ru.itdrive.jdbc.models.Lesson;
import ru.itdrive.jdbc.models.Student;
import ru.itdrive.jdbc.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class MainForRepositories {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "password";

    public static void main(String[] args) throws Exception {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        StudentsRepository repository = new StudentsRepositoryJdbcImpl(connection);
        CourseRepository repositoryCourse = new CourseRepositoryJdbcImpl(connection);
        LessonRepository repositoryLesson = new LessonRepositoryJdbcImpl(connection);

        List<Student> students = repository.findAll();
        System.out.println(students);
        System.out.println("-------");
        List<Course> courses = repositoryCourse.findAll();
        System.out.println(courses);
        System.out.println("-------");
        Course course = repositoryCourse.find(1);
        System.out.println(course);
        System.out.println("-------");
        Lesson lesson = repositoryLesson.find(1);
        System.out.println(lesson);
        List<Lesson> lessons = repositoryLesson.findAll();
        System.out.println(lessons);
    }
}
