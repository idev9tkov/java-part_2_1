package ru.itdrive.jdbc.models;

import java.util.List;

public class Course {
    private Integer id;
    private List<Lesson> lessons;

    public Course(Integer id, List<Lesson> lessons) {
        this.id = id;
        this.lessons = lessons;
    }

    public Course() {
    }

    public Course(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", lessons=" + lessons +
                '}';
    }
}
