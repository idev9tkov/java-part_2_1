package ru.itdrive.jdbc.models;

public class Lesson {
    private Integer id;
    private String name;
    private Integer course_id;

    public Lesson() {
    }

    public Lesson(Integer id, String name, Integer course_id) {
        this.id = id;
        this.name = name;
        this.course_id = course_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", course_id=" + course_id +
                '}';
    }
}
