package ru.itdrive.filesinfo.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileTree {
    public void filesTour(String path) {
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            List<String> result = walk
                    .map(Path::toString)
                    .collect(Collectors.toList());
            for (String val : result) {
                File file = new File(val);
                if (file.isDirectory()) {
                    System.out.println(file.getAbsolutePath());
                } else {
                    long size = file.length();
                    System.out.println(file.getAbsolutePath() + " = " + size + " bytes");
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}

