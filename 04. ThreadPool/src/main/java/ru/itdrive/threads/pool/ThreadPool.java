package ru.itdrive.threads.pool;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {

    private PoolWorker threads[];

    private final Deque<Runnable> tasks;

    public ThreadPool(int threadsCount) {
        this.tasks = new LinkedList<>();
        this.threads = new PoolWorker[threadsCount];
        for (int i = 0; i < threads.length; i++) {
            this.threads[i] = new PoolWorker();
            this.threads[i].start();
        }
    }

    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    private class PoolWorker extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                }
                Runnable task = tasks.pop();
                task.run();
            }
        }
    }
}
