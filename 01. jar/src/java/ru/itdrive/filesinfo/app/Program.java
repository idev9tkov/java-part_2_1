package ru.itdrive.filesinfo.app;
import ru.itdrive.filesinfo.utils.FileTree;
import com.beust.jcommander.JCommander;

public class Program {
    public static void main(String[] args) {
        Args arguments = new Args();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        FileTree fileTree = new FileTree();
        fileTree.filesTour(arguments.path);
    }
}
