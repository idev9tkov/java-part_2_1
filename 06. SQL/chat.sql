create table users
(
    id serial primary key,
    first_name char(20),
    last_name char(40),
    email char(40),
    password char(40),
    chat_id integer,
    foreign key (chat_id) references chat(id)
);

create table chat
(
    id serial primary key,
    name char(20),
    start_date timestamp
);

create table messages
(
    id serial primary key,
    text char(1000),
    users_id integer,
    chat_id integer,
    foreign key (users_id) references users(id),
    foreign key (chat_id) references chat(id)
);
