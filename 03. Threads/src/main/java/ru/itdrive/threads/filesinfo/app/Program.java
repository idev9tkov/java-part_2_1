package ru.itdrive.threads.filesinfo.app;
import ru.itdrive.threads.filesinfo.utils.FileInformation;
import ru.itdrive.threads.filesinfo.utils.FileTree;
import ru.itdrive.threads.filesinfo.utils.ThreadService;

import com.beust.jcommander.JCommander;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Program {
    public static void main(String[] args) {
        Map<String, List<FileInformation>> map = new ConcurrentHashMap<>();
        Args arguments = new Args();
        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);
        String[] str = FileTree.listFolders(arguments.path);
        ThreadService threadService = new ThreadService();
        for (int i = 0; i < str.length; i++) {
            int finalI = i;
           Runnable runnable = (() -> {
               FileTree fileTree = new FileTree();
               List<FileInformation> result = fileTree.filesTour(str[finalI]);
               map.put(str[finalI], result);
            });
            threadService.submit(runnable);
        }
        for (Map.Entry<String, List<FileInformation>> val : map.entrySet()) {
            System.out.println(val.getKey());
            System.out.println(val.getValue());
        }
    }
}
