package ru.itdrive.threads.filesinfo.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileTree {

    public List<FileInformation> filesTour(String path) {
        List<FileInformation> fileInformations = new ArrayList<>();
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            List<String> result = walk
                    .map(Path::toString)
                    .collect(Collectors.toList());
            for (String val : result) {
                File file = new File(val);
                String name = file.getPath();
                if (file.isDirectory()) {
                    fileInformations.add(new FileInformation(name, 0));
                } else {
                    long size = file.length();
                    fileInformations.add(new FileInformation(name, size));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return fileInformations;
    }

    public static String[] listFolders(String path) {
        String[] rsl;
        if (path.contains("-")) {
            rsl = path.split("-");
        } else {
            rsl = new String[]{path};
        }
        return rsl;
    }
}

