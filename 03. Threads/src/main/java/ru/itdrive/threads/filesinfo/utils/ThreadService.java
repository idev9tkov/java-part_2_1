package ru.itdrive.threads.filesinfo.utils;

public class ThreadService {
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
