package ru.itdrive.threads.filesinfo.utils;

public class FileInformation {
    private long size;
    private String name;

    public FileInformation() {
    }

    public FileInformation(String name, long size) {
        this.name = name;
        this.size = size;
    }

    public long getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FileInformation{" +
                "size=" + size +
                ", name='" + name + '\'' +
                '}';
    }
}
