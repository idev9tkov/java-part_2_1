package ru.itdrive.threads.filesinfo.app;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
@Parameters(separators = "=")
public class Args {
    @Parameter(names = {"--path"})
    public String path;
}
