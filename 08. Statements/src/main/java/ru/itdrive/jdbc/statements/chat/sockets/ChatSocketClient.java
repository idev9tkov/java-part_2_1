package ru.itdrive.jdbc.statements.chat.sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatSocketClient {
    private String userName;
    private String roomName;
    private Socket client;

    private PrintWriter toServer;
    private BufferedReader fromServer;

    public ChatSocketClient(String userName, String roomName, Socket client) {
        this.userName = userName;
        this.roomName = roomName;
        try {
            this.client = new Socket("127.0.0.1", 8080);
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessagesTask).start();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }

    private Runnable receiverMessagesTask = new Runnable() {
        public void run() {
            while (true) {
                try {
                    String messageFromServer = fromServer.readLine();
                    if (messageFromServer != null) {
                        System.out.println(messageFromServer);
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };

    public String getUserName() {
        return userName;
    }

    public Socket getClient() {
        return client;
    }

    public String getRoomName() {
        return roomName;
    }
}
