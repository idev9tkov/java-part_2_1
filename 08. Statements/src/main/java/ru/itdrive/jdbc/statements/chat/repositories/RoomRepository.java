package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Room;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room> {
    void save(Room object);

    void update(Room object);

    void delete(Integer id);

    Room find(Integer id);

    List<Room> findAll();
}
