package ru.itdrive.jdbc.statements.chat.sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class SocketServer {

    private Socket socketClient;
    private List<ChatSocketClient> socketClients = new ArrayList<>();

    public void start() {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(8080);
                try {
                    while (true) {

                        socketClient = serverSocket.accept();
                        Scanner scanner = new Scanner(System.in);
                        System.out.println("Enter your name:");
                        String userName = scanner.nextLine();
                        System.out.println("Enter room name:");
                        String roomName = scanner.nextLine();

                        ChatSocketClient chatSocketClient = new ChatSocketClient(userName, roomName, socketClient);
                        socketClients.add(chatSocketClient);
                        new Thread(() -> {
                            try {
                                InputStream clientInputStream = socketClient.getInputStream();
                                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
                                String inputLine = clientReader.readLine();
                                while (inputLine != null) {
                                    for (ChatSocketClient val : socketClients) {
                                        if (val.getRoomName().equals(chatSocketClient.getRoomName())) {
                                            Socket socket = val.getClient();
                                            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                                            writer.println(inputLine);
                                        }
                                        inputLine = clientReader.readLine();
                                    }
                                }
                            } catch (IOException e) {
                                throw new IllegalArgumentException(e);
                            }
                        }).start();
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
