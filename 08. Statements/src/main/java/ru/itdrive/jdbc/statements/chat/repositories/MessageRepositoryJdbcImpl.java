package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Message;

import java.util.List;

public class MessageRepositoryJdbcImpl implements MessageRepository {
    @Override
    public void save(Message object) {

    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Message find(Integer id) {
        return null;
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public List<Message> findLastThirty() {
        return null;
    }
}
