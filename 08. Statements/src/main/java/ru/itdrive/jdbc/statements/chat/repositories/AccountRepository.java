package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Account;

import java.util.List;

public interface AccountRepository extends CrudRepository<Account> {
    void save(Account object);

    void update(Account object);

    void delete(Integer id);

    Account find(Integer id);

    List<Account> findAll();
}
