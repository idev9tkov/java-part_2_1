package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Account;

import java.util.List;

public class AccountRepositoryJdbcImpl implements AccountRepository {
    @Override
    public void save(Account object) {

    }

    @Override
    public void update(Account object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Account find(Integer id) {
        return null;
    }

    @Override
    public List<Account> findAll() {
        return null;
    }
}
