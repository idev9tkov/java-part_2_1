package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Message;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message> {
    void save(Message object);

    void update(Message object);

    void delete(Integer id);

    Message find(Integer id);

    List<Message> findAll();

    List<Message> findLastThirty();
}
