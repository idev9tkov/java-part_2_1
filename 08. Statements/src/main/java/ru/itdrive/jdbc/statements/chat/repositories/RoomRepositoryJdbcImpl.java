package ru.itdrive.jdbc.statements.chat.repositories;

import ru.itdrive.jdbc.statements.chat.models.Room;

import java.util.List;

public class RoomRepositoryJdbcImpl implements RoomRepository {
    @Override
    public void save(Room object) {

    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Room find(Integer id) {
        return null;
    }

    @Override
    public List<Room> findAll() {
        return null;
    }
}
