package ru.itdrive.jdbc.statements.chat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Message {
    private Integer id;
    private String text;
    private Integer account_id;
    private Integer room_id;
}
